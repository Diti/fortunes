This is a (personal) collection of quotes (in [`fortune`][w:fortune] format)
from the following books:

 - The [Guardians of Ga'Hoole][w:GaHoole] series

Installation
============

For now, you have to do it manually, by running `[sudo] make install`.  
Internally, it goes through two steps:

  1. `make` creates the index files that `fortune` needs, then
  2. `make install` copies the quotes to `/usr/share/games/fortunes`.

Example
=======

    $ fortune gahoole
    Gylf, you can practice forever and still never fly if you do not really believe you can.
    
    	— Gylfie’s father, “Guardians of Ga’Hoole #1: The Capture”

What's next?
============

A lot more `gahoole` quotes have yet to be added. Including songs & poems.  
Another series of books to be added is the [Silverwing][w:Silverwing] series.

Please **star** the project on GitHub if you want to show your interest.  
I might make a Debian/Ubuntu package for this (=> quotes always up-to-date).


[w:fortune]:    https://en.wikipedia.org/wiki/Fortune_%28Unix%29
[w:GaHoole]:    https://en.wikipedia.org/wiki/Guardians_of_Ga%27Hoole
[w:Silverwing]: https://en.wikipedia.org/wiki/Silverwing_%28series%29
