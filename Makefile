## ########################################################################## ##
##                                                                            ##
##                                                [www]               The     ##
##   Makefile                                      diti.me          name's>   ##
##                                                                Diti. I     ##
##   By: Dimitri Torterat <kra@diti.me>           [PGP]          like pen-    ##
##                                                 CD42FF00     guins \and    ##
##   Created: 2014-05-05 by diti                                owls. #`-'    ##
##   Updated: 2014-05-05 by diti                              ,;;  `#\.       ##
##                                                                            ##
## ########################################################################## ##

# Let's do this the stupid way first: manually listing the fortunes
FILES = gahoole

FORTUNES_DIR = fortunes
FORTUNES_DESTDIR := /usr/share/games/fortunes

INSTALL := install
INSTALL_DATA := $(INSTALL) -m 644 -p

FORTUNES_FILES = $(addprefix $(FORTUNES_DIR)/, $(FILES))
FORTUNES_DATAFILES = $(addsuffix .dat, $(FORTUNES_FILES))

.PHONY: all
all: $(FORTUNES_DATAFILES)

$(FORTUNES_DIR)/%.dat : $(FORTUNES_DIR)/%
	@printf "Processing file \`%s\` -> \`%s\`\n" $< $@
	@strfile -s $< $@

.PHONY: install uninstall
install: $(FORTUNES_FILES) $(FORTUNES_DATAFILES)
	$(INSTALL_DATA) -t $(DESTDIR)$(FORTUNES_DESTDIR)/ $<.dat $<

uninstall: $(addprefix $(FORTUNES_DESTDIR)/, $(FILES))
	$(RM) $^.dat $^

.PHONY: clean distclean
clean distclean:
	$(RM) $(FORTUNES_DATAFILES)
